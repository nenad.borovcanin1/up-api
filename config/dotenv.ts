import { config } from "dotenv";

export function initializeEnvironmentVariables() {

    config();

    console.log("Env:", process.env.NODE_ENV);
}
