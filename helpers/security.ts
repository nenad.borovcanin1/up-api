import { Response } from "express";
import {ResponseBuilder} from "./utilities/response";
import {error_help_url_root, statuses} from "./messages";

/**
 * Error response with logging functionality
 *
 * @param response
 * @param status
 * @param errors
 * @returns {Response}
 */
export function errorResponse(response: Response, status: number = statuses.success, errors: object[]): Response {

    const errorResult = [];

    errors
        .map((error: Error) => {

            if ("name" in error) {

                errorResult.push({
                    code: error.name.toUpperCase(),
                    help_url: `${error_help_url_root}${error.name.toLowerCase()}`,
                    message: error.message,
                });

            } else {

                errorResult.push(error);
            }
        });

    return new ResponseBuilder<any>()
        .setStatus(false)
        .setResponse(response)
        .setResponseStatus(status)
        .setError(errorResult)
        .build();
}
