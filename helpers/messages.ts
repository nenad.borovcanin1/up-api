export const error_help_url_root = "users-service/";

export interface IMessage {
    code: string | number;
    help_url: string;
    message: string;
}

export interface IMessageData {
    invalid_request: IMessage;
}

export const messages: IMessageData = {
    invalid_request: {
        code: "INVALID_REQUEST",
        help_url: `${error_help_url_root}invalid_request`,
        message: "Invalid request",
    },
};

export interface IStatuses {
    forbidden: number;
    invalid: number;
    success: number;
    created: number;
    server_error: number;
    not_found: number;
    too_many_requests: number;
    unauthorized: number;
}

export const statuses: IStatuses = {
    forbidden: 403,
    invalid: 400,
    not_found: 404,
    server_error: 500,
    success: 200,
    created: 201,
    too_many_requests: 427,
    unauthorized: 401,
};
