import {Request, Response, Router} from "express";
import {statuses} from "../../../../helpers/messages";
import {errorResponse} from "../../../../helpers/security";
import {ResponseBuilder} from "../../../../helpers/utilities/response";
import {UsersController} from "../../../controllers/users/users.controller";

export class UserRoutes {
    private router: Router = Router();

    public getRouter() {
        this.router.get("/getData", async (request: Request, response: Response) => {
            try {

                const data = UsersController.getUsers();
                return new ResponseBuilder<Array<object>>()
                    .setData(data)
                    .setResponse(response)
                    .setMeta({total: data.length})
                    .setResponseStatus(statuses.success)
                    .build();

            } catch (e) {
                return errorResponse(response, statuses.invalid, [e]);
            }
        });
        return this.router;
    }
}
