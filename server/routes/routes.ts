import {Application} from "express";
import {apiRootBaseV1} from "../../config/config";
import {UserRoutes} from "./v1/users";

export async function apiRoutes(app: Application) {
    app.use(apiRootBaseV1, new UserRoutes().getRouter());
}
