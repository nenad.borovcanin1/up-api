import {initializeEnvironmentVariables} from "../config/dotenv";

initializeEnvironmentVariables();
import * as express from "express";

import {apiRoutes} from "./routes/routes";

const app: express.Application = express();
app.listen(process.env.PORT);

apiRoutes(app).catch((error) => console.log(error));
